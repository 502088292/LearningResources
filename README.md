# 
工具						| 相关链接 | 支持平台
----						| ---- | ----
Github Desktop`推荐！` 	| [https://desktop.github.com](https://desktop.github.com) 	| `Windwos` `Mac`
git-scm 					| [http://git-scm.com/download/](http://git-scm.com/download/) 		| `Windwos` `Mac` `Linux` `Solaris`
SourceTree				| [https://www.sourcetreeapp.com](https://www.sourcetreeapp.com) 	| `Windows` `Mac`
SmartGit`开源` 			| [http://www.syntevo.com/smartgithg/](http://www.syntevo.com/smartgithg/) | `Windwos` `Mac` `Linux`
GitNub`开源` 				| [https://github.com/Caged/gitnub/wiki](https://github.com/Caged/gitnub/wiki) | `Mac`
Tower`收费` 				| [http://www.git-tower.com](http://www.git-tower.com) 				| `Mac`
Gitbox`收费` 				| [http://gitboxapp.com](http://gitboxapp.com) | `Mac`

工具						| 相关链接 
----						| ---- 
swift注释说明		| [http://nshipster.cn/swift-documentation/](http://nshipster.cn/swift-documentation/)
命令行的艺术		| [https://github.com/jlevy/the-art-of-command-line/blob/master/README-zh.md](https://github.com/jlevy/the-art-of-command-line/blob/master/README-zh.md)


# 语言的学习
语言		|相关连接
--------			|--------
swift|[http://wiki.jikexueyuan.com/project/swift/chapter1/02_a_swift_tour.html#690358359a581e6875d7b9875a1e07d3](http://wiki.jikexueyuan.com/project/swift/chapter1/02_a_swift_tour.html#690358359a581e6875d7b9875a1e07d3)
git教程 |[http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000](http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)